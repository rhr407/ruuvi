# Gateway Project based on ESP32 for RUUVI Tags

This code is dedicated for esp32 based gateway that connects RUUVI Tags with AWS-IoT core.

Along with other header bytes this code sends the following data related to RUUVI Tags in raw format with topic name `Ruuvi/{MAC_ADDRESS}`

Here the MAC_ADDRESS corresponds to ESP32 (in my case it's ruuvi/c41705a4ae30)

```	
{TIMESTAMP}:{RSSI}:{PAYLOAD}
```	

In order to test the code

1. Connect the esp32 dev-kit to your PC.  

2. Go into the project folder, open terminal in the folder path and run the following command: 

```	
make menuconfig
```	

2. Select the ‘Project Configuration’ option and set the GPIO pin numbers for WiFi status LED and WiFi reset configuration GPIOs and AWS-IoT certificate selection manually as per your requirements.

3. Exit and save the configuration.

4. Run the following command: 

```	
make erase_flash flash monitor
```

This will erase the flash and burn the firmware. You can see the output from the esp32 on this terminal.

5. When the code is burnt the LED on the dev-kit will start blinking and the esp32 will be showing the following output, which means it is in APSTA mode:

```
wifi_manager: starting softAP with ssid esp32
wifi_manager: starting softAP with 20 MHz bandwidth
wifi_manager: starting softAP on channel 5
SYSTEM_EVENT_STA_START*****************************
wifi_manager: STA power save enabled
SYSTEM_EVENT_AP_START*****************************
wifi_mamager: softAP started, starting http_server
http_server: received start bit, starting server
HTTP Server listening...
```

6. Now it’s time to connect to esp32 access point. The password is `esp32pwd`.

7. When esp32 is connected to your PC go to the browser and enter `192.168.1.1`, wait for some time as esp32 is loading APs and then select the desired Access Point (AP) to which esp32 will connect and come into STA only mode. You will not be able to access the esp32 as an AP unless you press the wifi reset button (selected GPIO in `maek menuconfig`). The WiFi status LED will start blinking which means that the esp32 is not yet connected to WiFi. When the esp32 is connected to the desired AP the LED will get stable.

8. When you press the GPIO wifi reset button the esp32 will go into APSTA mode and you can again repeat the point 7.

9. If you press the dev-kit reset button then esp32 will reload the previously saved AP settings from NVSRAM and connect to it. So LED will again get stable when esp32 is connected to WiFi AP.

10. You need to create your own `certificate.pem.crt` and `private.pem.key` files from aws-iot by following [this link](https://docs.aws.amazon.com/iot/latest/developerguide/iot-gs.html) and put them in `/main/certs` folder in this project along with `aws-root-ca.pem`.


# Tasks List

All the following tasks have been completed as per the requirements set:

- [x] WiFi status LED (GPIO selection from `menuconfig`)
- [x] WiFi Reset button (GPIO selection from `menuconfig`)
- [x] Reset hysterisis for 10 seconds
- [x] MQTT configuration set in `menuconfig`
- [x] Web page to connect to select among different APs


# Downloading this Repo
Use the following command on your terminal to downlaod this repository.
```
git clone https://gitlab.com/rhr407/ruuvi

```
