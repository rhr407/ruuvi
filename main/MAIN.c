/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * Additions Copyright 2016 Espressif Systems (Shanghai) PTE LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
/**
 * @file subscribe_publish_sample.c
 * @brief simple MQTT publish and subscribe on the same topic
 *
 * This example takes the parameters from the build configuration and establishes a connection to the AWS IoT MQTT Platform.
 * It subscribes and publishes to the same topic - "test_topic/esp32"
 *
 * Some setup is required. See example README for details.
 *
 */


#include "common.h"


static const char* RUUVI_TAG = "RUUVI-TAGS";
static const char* AWS_IoT_TAG = "AWS_IoT";
static const char* TIME_and_DATE_TAG = "TIME_and_DATE";
static const char* WiFi_TAG = "WiFi";


extern esp_ble_ruuvi_vendor_t vendor_config;

char cPayload[200];

AWS_IoT_Client client;

IoT_Error_t rc;


///Declare static functions
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);

esp_ble_ruuvi_t *ruuvi_data;
esp_ble_gap_cb_param_t *scan_result;

char ble_flag = 0;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;
extern esp_ble_ruuvi_vendor_t vendor_config;

char cPayload[200];

char TOPIC[18];
int TOPIC_LEN;

AWS_IoT_Client client;

IoT_Error_t rc;

static void obtain_time(void);
static void initialize_sntp(void);

void get_time_task(void *param);


uint16_t pkt_cnt = 1;

char strftime_buf[64];

uint8_t *ble_dev_addr;

int ble_dev_rssi = 0;

/**
 * @brief Default MQTT HOST URL is pulled from the aws_iot_config.h
 */
/**
 * @brief Default MQTT port is pulled from the aws_iot_config.h
 */
uint32_t port = AWS_IOT_MQTT_PORT;



///Declare static functions
static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);

esp_ble_ruuvi_t *ruuvi_data;
esp_ble_gap_cb_param_t *scan_result;

/*-------------------------------------------------------------------------------------------------*/

#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds
/*
 * A sample structure to pass events
 * from the timer interrupt handler to the main program.
 */
typedef struct {
    int type;  // the type of timer's event
    int timer_group;
    int timer_idx;
    uint64_t timer_counter_value;
} timer_event_t;

/*OUTPUT GPIOs--------------------------------------------------------------*/

#define GPIO_OUTPUT_PINS_SEL_MASK                     1ULL<<CONFIG_LED_GPIO

/*INPUT GPIOs--------------------------------------------------------------*/

#define GPIO_WIFI_RESET_BUTTON_MASK                   (1ULL<<CONFIG_WIFI_RESET_BUTTON_GPIO)

#define ESP_INTR_FLAG_DEFAULT                         0

/*-------------------------------------------------------------------------*/

static xQueueHandle gpio_evt_queue = NULL;
static xQueueHandle timer_queue = NULL;

/* FreeRTOS event group to signal when we are connected & ready to make a request */
// static EventGroupHandle_t wifi_event_group;

static TaskHandle_t task_http_server = NULL;
static TaskHandle_t task_wifi_manager = NULL;

static void config_timer(void);
esp_err_t _http_event_handler(esp_http_client_event_t *evt);


/* The event group allows multiple bits for each event,
 but we only care about one event - are we connected
 to the AP with an IP? */

int32_t i = 0;
IoT_Publish_Message_Params paramsQOS0;
IoT_Publish_Message_Params paramsQOS1;

void init_gpio(void);

AWS_IoT_Client client;
IoT_Error_t rc = FAILURE;

esp_err_t esp_task_wdt_reset();



/* CA Root certificate, device ("Thing") certificate and device
 * ("Thing") key.

 Example can be configured one of two ways:

 "Embedded Certs" are loaded from files in "certs/" and embedded into the app binary.

 "Filesystem Certs" are loaded from the filesystem (SD card, etc.)

 See example README for more details.
 */
#if defined(CONFIG_EXAMPLE_EMBEDDED_CERTS)

extern const uint8_t aws_root_ca_pem_start[] asm("_binary_aws_root_ca_pem_start");
extern const uint8_t aws_root_ca_pem_end[] asm("_binary_aws_root_ca_pem_end");
extern const uint8_t certificate_pem_crt_start[] asm("_binary_certificate_pem_crt_start");
extern const uint8_t certificate_pem_crt_end[] asm("_binary_certificate_pem_crt_end");
extern const uint8_t private_pem_key_start[] asm("_binary_private_pem_key_start");
extern const uint8_t private_pem_key_end[] asm("_binary_private_pem_key_end");

#elif defined(CONFIG_EXAMPLE_FILESYSTEM_CERTS)

static const char * DEVICE_CERTIFICATE_PATH = CONFIG_EXAMPLE_CERTIFICATE_PATH;
static const char * DEVICE_PRIVATE_KEY_PATH = CONFIG_EXAMPLE_PRIVATE_KEY_PATH;
static const char * ROOT_CA_PATH = CONFIG_EXAMPLE_ROOT_CA_PATH;

#else
#error "Invalid method for loading certs"
#endif

/**
 * @brief Default MQTT HOST URL is pulled from the aws_iot_config.h
 */
char HostAddress[255] = AWS_IOT_MQTT_HOST;

/**
 * @brief Default MQTT port is pulled from the aws_iot_config.h
 */



void aws_iot_task(void *param);


void iot_subscribe_callback_handler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen,
                                    IoT_Publish_Message_Params *params, void *pData) {
    ESP_LOGI(AWS_IoT_TAG, "%.*s\t%.*s", topicNameLen, topicName, (int) params->payloadLen, (char *)params->payload);

    // ESP_LOGI(AWS_IoT_TAG, "Published");
    // esp_log_buffer_hex("PACKET: ", (char *)params->payload, (int) params->payloadLen );

}


void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data) {
    ESP_LOGW(AWS_IoT_TAG, "MQTT Disconnect");
    rc = FAILURE;

    if (NULL == pClient) {
        return;
    }

    if (aws_iot_is_autoreconnect_enabled(pClient)) {
        ESP_LOGI(AWS_IoT_TAG, "Auto Reconnect is enabled, Reconnecting attempt will start now");
    } else {
        ESP_LOGW(AWS_IoT_TAG, "Auto Reconnect not enabled. Starting manual reconnect...");
        rc = aws_iot_mqtt_attempt_reconnect(pClient);
        if (NETWORK_RECONNECTED == rc) {
            ESP_LOGW(AWS_IoT_TAG, "Manual Reconnect Successful");
        } else {
            ESP_LOGW(AWS_IoT_TAG, "Manual Reconnect Failed - %d", rc);
        }
    }
}



void aws_iot_task(void *param) {

    IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
    IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;


    ESP_LOGI(AWS_IoT_TAG, "AWS IoT SDK Version %d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

    mqttInitParams.enableAutoReconnect = false; // We enable this later below
    mqttInitParams.pHostURL = HostAddress;
    mqttInitParams.port = port;



#if defined(CONFIG_EXAMPLE_EMBEDDED_CERTS)
    mqttInitParams.pRootCALocation = (const char *)aws_root_ca_pem_start;
    mqttInitParams.pDeviceCertLocation = (const char *)certificate_pem_crt_start;
    mqttInitParams.pDevicePrivateKeyLocation = (const char *)private_pem_key_start;

#elif defined(CONFIG_EXAMPLE_FILESYSTEM_CERTS)
    mqttInitParams.pRootCALocation = ROOT_CA_PATH;
    mqttInitParams.pDeviceCertLocation = DEVICE_CERTIFICATE_PATH;
    mqttInitParams.pDevicePrivateKeyLocation = DEVICE_PRIVATE_KEY_PATH;
#endif

    mqttInitParams.mqttCommandTimeout_ms = 20000;
    mqttInitParams.tlsHandshakeTimeout_ms = 5000;
    mqttInitParams.isSSLHostnameVerify = true;
    mqttInitParams.disconnectHandler = disconnectCallbackHandler;
    mqttInitParams.disconnectHandlerData = NULL;

#ifdef CONFIG_EXAMPLE_SDCARD_CERTS
    ESP_LOGI(AWS_IoT_TAG, "Mounting SD card...");
    sdmmc_host_t host = SDMMC_HOST_DEFAULT();
    sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();
    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
        .format_if_mount_failed = false,
        .max_files = 3,
    };
    sdmmc_card_t* card;
    esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
    if (ret != ESP_OK) {
        ESP_LOGE(AWS_IoT_TAG, "Failed to mount SD card VFAT filesystem.");
        abort();
    }
#endif

    rc = aws_iot_mqtt_init(&client, &mqttInitParams);
    if (SUCCESS != rc) {
        ESP_LOGE(AWS_IoT_TAG, "aws_iot_mqtt_init returned error : %d ", rc);
        abort();
    }

    /*---------------------------------------------------------------------------------*/

    connectParams.keepAliveIntervalInSec = 10;
    connectParams.isCleanSession = true;
    connectParams.MQTTVersion = MQTT_3_1_1;

    /* Client ID is set in the menuconfig of the example */
    connectParams.pClientID = CONFIG_AWS_EXAMPLE_CLIENT_ID;
    connectParams.clientIDLen = (uint16_t) strlen(CONFIG_AWS_EXAMPLE_CLIENT_ID);

    /*---------------------------------------------------------------------------------*/
    connectParams.isWillMsgPresent = false;

    /*---------------------------------------------------------------------------------*/
    ESP_LOGI(AWS_IoT_TAG, "Connecting to AWS...");
    do {
        rc = aws_iot_mqtt_connect(&client, &connectParams);
        if (SUCCESS != rc) {
            ESP_LOGE(AWS_IoT_TAG, "Error(%d) connecting to %s:%d", rc, mqttInitParams.pHostURL, mqttInitParams.port);
            vTaskDelay(1000 / portTICK_RATE_MS);
        }
    } while (SUCCESS != rc);

    /*
     * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
     *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
     *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
     */
    rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
    if (SUCCESS != rc) {
        ESP_LOGE(AWS_IoT_TAG, "Unable to set Auto Reconnect to true - %d", rc);
        abort();
    }


    uint8_t mac_address[6];

    esp_efuse_mac_get_default(mac_address);

    sprintf(TOPIC, "%s%02x%02x%02x%02x%02x%02x", "ruuvi/", (int)mac_address[5], (int)mac_address[4], (int)mac_address[3], (int)mac_address[2], (int)mac_address[1], (int)mac_address[0] );

    TOPIC_LEN = strlen(TOPIC);


    ESP_LOGI(AWS_IoT_TAG, "Subscribing...");
    rc = aws_iot_mqtt_subscribe(&client, TOPIC, TOPIC_LEN, QOS0, iot_subscribe_callback_handler, NULL);

    if (SUCCESS != rc) {
        ESP_LOGE(AWS_IoT_TAG, "Error subscribing : %d ", rc);
        abort();
    }

    sprintf(cPayload, "%s : %d ", "hello from SDK", i);

    paramsQOS0.qos = QOS0;
    paramsQOS0.payload = (void *) cPayload;
    paramsQOS0.isRetained = 0;

    // paramsQOS1.qos = QOS1;
    // paramsQOS1.payload = (void *) cPayload;
    // paramsQOS1.isRetained = 0;

    while ((NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc)) {

        //Max time the yield function will wait for read messages
        rc = aws_iot_mqtt_yield(&client, 100);
        if (NETWORK_ATTEMPTING_RECONNECT == rc) {
            // If the client is attempting to reconnect we will skip the rest of the loop.
            continue;
        }

        if (ble_flag) {

            ble_flag = 0;

            sprintf(cPayload, "%s:%s:%d:%s:%d ",  strftime_buf, (char*)ble_dev_addr, ble_dev_rssi, (char*) ruuvi_data, pkt_cnt++);

            if (pkt_cnt >= 65534) pkt_cnt = 1;

            paramsQOS0.payloadLen = strlen(cPayload);

            rc = aws_iot_mqtt_publish(&client, TOPIC, TOPIC_LEN, &paramsQOS0);
        }
        esp_task_wdt_reset();

        // vTaskDelay(20 / portTICK_RATE_MS);


    }

    ESP_LOGE(AWS_IoT_TAG, "An error occurred in the main loop.");
    abort();
}

/*========================== Getting updated time from the Internet =======================================*/

void get_time_task(void *param)
{
    time_t now;
    struct tm timeinfo;

    for (;;) {

        time(&now);
        localtime_r(&now, &timeinfo);
        // Is time set? If not, tm_year will be (1970 - 1900).
        if (timeinfo.tm_year < (2016 - 1900)) {
            ESP_LOGI(TIME_and_DATE_TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
            obtain_time();
            // update 'now' variable with current time
            time(&now);
        }

        setenv("TZ", "UTC", 1);
        tzset();
        localtime_r(&now, &timeinfo);
        strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
        // ESP_LOGI(TIME_and_DATE_TAG, "Time: %s", strftime_buf);

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }

}

static void obtain_time(void)
{
    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry = 0;
    const int retry_count = 10;
    while (timeinfo.tm_year < (2016 - 1900) && ++retry < retry_count) {
        ESP_LOGI(TIME_and_DATE_TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        time(&now);
        localtime_r(&now, &timeinfo);
    }
}

static void initialize_sntp(void)
{
    ESP_LOGI(TIME_and_DATE_TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();
}
/*---------------------------------------------------------------------------*/
/*----------------------------- GPIO Settings -------------------------------*/
/*---------------------------------------------------------------------------*/
static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

/*----------This I am doing for 10 sec Reset ---------------------*/
void IRAM_ATTR timer_isr(void *para)
{
    uint32_t timer_flag = 1;
    xQueueSendFromISR(timer_queue, &timer_flag, NULL);
    // printf("WIFI Reset Activated......**********\n");
    TIMERG0.int_clr_timers.t0 = 1;
    // wifi_manager_disconnect_async();
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void timer_task(void *arg)
{
    uint32_t timer_flag;

    for (;;)  {
        if (xQueueReceive(timer_queue, &timer_flag, portMAX_DELAY)) {

            printf("WIFI Reset Activated......**********\n");

            wifi_manager_disconnect_async();
        }

    }
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void blink_LED_task(void *pvParameter)
{
    for (;;)  {
        /* Blink off (output low) */
        gpio_set_level(CONFIG_LED_GPIO, 0);
        vTaskDelay(150 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        gpio_set_level(CONFIG_LED_GPIO, 1);
        vTaskDelay(150 / portTICK_PERIOD_MS);
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void gpio_task_example(void* arg)
{
    uint32_t io_num;
    char timer_started = 0;

    for (;;) {
        if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {

            if (io_num == CONFIG_WIFI_RESET_BUTTON_GPIO)
            {
                if (timer_started == 0 && gpio_get_level(io_num) == 0)
                {
                    printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level(io_num));
                    printf("Timer Started......**********\n");

                    // Start the timer

                    timer_start(TIMER_GROUP_0, TIMER_0);

                    timer_started = 1;
                }

                else
                {
                    // Stop and reinitialize the timer

                    config_timer();

                    timer_started = 0;
                }

            }
        }
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

void init_gpio(void) {

    gpio_pad_select_gpio(CONFIG_LED_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(CONFIG_LED_GPIO, GPIO_MODE_OUTPUT);

    gpio_set_level(CONFIG_LED_GPIO, 0);

    /*OUTPUT GPIOs-----------------------------------------------------------------*/

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO2 for LED
    io_conf.pin_bit_mask = GPIO_OUTPUT_PINS_SEL_MASK;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

    /*INPUT GPIO WIFI_RESET_BUTTON  -------------------------------------*/
    //interrupt of rising edge

    io_conf.intr_type = GPIO_PIN_INTR_ANYEDGE; //GPIO_PIN_INTR_POSEDGE; Rizwan
    //bit mask of the pins
    io_conf.pin_bit_mask = GPIO_WIFI_RESET_BUTTON_MASK;
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);


    /*-------------------------------------------------------------------*/
    //create a queue to handle gpio event from isr
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    // gpio_isr_handler_add(CONFIG_CURRENT_PROTECTION_GPIO, gpio_isr_handler, (void*) CONFIG_CURRENT_PROTECTION_GPIO);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(CONFIG_WIFI_RESET_BUTTON_GPIO, gpio_isr_handler, (void*) CONFIG_WIFI_RESET_BUTTON_GPIO);
}



/*
 * Initialize selected timer of the timer group 0
 *
 * timer_idx - the timer number to initialize
 * auto_reload - should the timer auto reload on alarm?
 * timer_interval_sec - the interval of alarm to set
 */
static void config_timer(void)
{
    /* Select and initialize basic parameters of the timer */
    timer_config_t config;
    config.divider = TIMER_DIVIDER;
    config.counter_dir = TIMER_COUNT_UP;
    config.counter_en = TIMER_PAUSE;
    config.alarm_en = TIMER_ALARM_EN;
    config.intr_type = TIMER_INTR_LEVEL;
    config.auto_reload = 0;


    timer_init(TIMER_GROUP_0, TIMER_0, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, 10 * TIMER_SCALE); // 10 seconds interval

    timer_enable_intr(TIMER_GROUP_0, TIMER_0);
    timer_isr_register(TIMER_GROUP_0, TIMER_0, timer_isr, (void *) TIMER_0, ESP_INTR_FLAG_IRAM, NULL);

}
/*========================== BLE =======================================*/


static esp_ble_scan_params_t ble_scan_params = {
    .scan_type              = BLE_SCAN_TYPE_ACTIVE,
    .own_addr_type          = BLE_ADDR_TYPE_PUBLIC,
    .scan_filter_policy     = BLE_SCAN_FILTER_ALLOW_ALL,
    .scan_interval          = 0x50,
    .scan_window            = 0x30,
    .scan_duplicate         = BLE_SCAN_DUPLICATE_DISABLE
};

static void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    esp_err_t err;

    esp_task_wdt_reset();


    switch (event) {
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT: break;
    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
        //the unit of the duration is second, 0 means scan permanently
        uint32_t duration = 0;
        esp_ble_gap_start_scanning(duration);
        break;
    }
    case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT: {
        //scan start complete event to indicate scan start successfully or failed
        if ((err = param->scan_start_cmpl.status) != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(RUUVI_TAG, "Scan start failed: %s", esp_err_to_name(err));
        }
        break;
    }
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT: {
        //adv start complete event to indicate adv start successfully or failed
        if ((err = param->adv_start_cmpl.status) != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(RUUVI_TAG, "Adv start failed: %s", esp_err_to_name(err));
        }
        break;
    }
    case ESP_GAP_BLE_SCAN_RESULT_EVT: {
        esp_ble_gap_cb_param_t *scan_result = (esp_ble_gap_cb_param_t *)param;
        switch (scan_result->scan_rst.search_evt) {
        case ESP_GAP_SEARCH_INQ_RES_EVT: {
            /* Search for BLE ruuvi Packet */

            if (esp_ble_is_ruuvi_packet(scan_result->scan_rst.ble_adv, scan_result->scan_rst.ble_evt_type)) {
                ruuvi_data = (esp_ble_ruuvi_t*)(scan_result->scan_rst.ble_adv);

                ble_dev_addr = scan_result->scan_rst.bda;

                ble_dev_rssi = scan_result->scan_rst.rssi;


                // esp_log_buffer_hex("RUUVI_TAGS: Device address:", ble_dev_addr, ESP_BD_ADDR_LEN );

                // ESP_LOGI(RUUVI_TAG, "RSSI: %d dbm\n", ble_dev_rssi);

                ble_flag = 1;
            }
            break;
        }

        default:            break;

        }
        break;
    }

    case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT: {
        if ((err = param->scan_stop_cmpl.status) != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(RUUVI_TAG, "Scan stop failed: %s", esp_err_to_name(err));
        }
        else {
            ESP_LOGI(RUUVI_TAG, "Stop scan successfully");
        }
        break;
    }

    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT: {
        if ((err = param->adv_stop_cmpl.status) != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(RUUVI_TAG, "Adv stop failed: %s", esp_err_to_name(err));
        }
        else {
            ESP_LOGI(RUUVI_TAG, "Stop adv successfully");
        }
        break;
    }

    default:        break;

    }
}



void ble_ruuvi_appRegister(void)
{
    esp_err_t status;

    ESP_LOGI(RUUVI_TAG, "register callback");

    //register the scan callback function to the gap module
    if ((status = esp_ble_gap_register_callback(esp_gap_cb)) != ESP_OK) {
        ESP_LOGE(RUUVI_TAG, "gap register error: %s", esp_err_to_name(status));
        return;
    }

}

void ble_ruuvi_init(void)
{
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));


    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    esp_bt_controller_init(&bt_cfg);
    esp_bt_controller_enable(ESP_BT_MODE_BLE);

    esp_bluedroid_init();
    esp_bluedroid_enable();
    ble_ruuvi_appRegister();

    /* set scan parameters */
    esp_ble_gap_set_scan_params(&ble_scan_params);
}


/*-------------------------------------------------------------------------------------*/

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGD(WiFi_TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}


void app_main()
{
    /*-----------------------------------------------------------------------------*/
    // Initialize NVS.
    esp_err_t err = nvs_flash_init();
    // OTA app partition table has a smaller NVS partition size than the non-OTA
    // partition table. This size mismatch may cause NVS initialization to fail.
    // If this happens, we erase NVS partition and initialize NVS again.
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    init_gpio();

    config_timer();

    // ble_ruuvi_init();

    /* disable the default wifi logging */
    esp_log_level_set("wifi", ESP_LOG_NONE);

    /* start the HTTP Server task */
    xTaskCreate(&http_server, "http_server", 2048, NULL, 5, &task_http_server);

    /* start the wifi manager task */
    xTaskCreate(&wifi_manager, "wifi_manager", 4096, NULL, 5, &task_wifi_manager);

    // start gpio task
    xTaskCreate(gpio_task_example, "gpio_task_example", 2048, NULL, 3, NULL);

    timer_queue = xQueueCreate(10, sizeof(timer_event_t));

    xTaskCreate(&timer_task, "timer_task", 2048, NULL, 0, NULL);



}
