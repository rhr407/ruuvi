#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>



#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"


#define RUUVI_SENDER      0
#define RUUVI_RECEIVER    1
#define RUUVI_MODE RUUVI_RECEIVER

/* Major and Minor part are stored in big endian mode in ruuvi packet,
 * need to use this macro to transfer while creating or processing
 * ruuvi data */
#define ENDIAN_CHANGE_U16(x) ((((x)&0xFF00)>>8) + (((x)&0xFF)<<8))

/* Espressif WeChat official account can be found using WeChat "Yao Yi Yao Zhou Bian", 
 * if device advertises using ESP defined UUID. 
 * Please refer to http://zb.weixin.qq.com for further information. */
#define ESP_UUID    {0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25}
#define ESP_MAJOR   10167
#define ESP_MINOR   61958


typedef struct {
    uint8_t flags[3];
    uint8_t length;
    uint8_t type;
    uint16_t company_id;
    uint16_t beacon_type;
}__attribute__((packed)) esp_ble_ruuvi_head_t;


typedef struct {
    uint8_t proximity_uuid[16];
    uint16_t major;
    uint16_t minor;
    int8_t measured_power;
}__attribute__((packed)) esp_ble_ruuvi_vendor_t;


typedef struct {
    esp_ble_ruuvi_head_t ruuvi_head;
    esp_ble_ruuvi_vendor_t ruuvi_vendor;
}__attribute__((packed)) esp_ble_ruuvi_t;


/* For ruuvi packet format, please refer to Apple "Proximity Beacon Specification" doc */
/* Constant part of ruuvi data */
extern esp_ble_ruuvi_head_t ruuvi_common_head;

bool esp_ble_is_ruuvi_packet (uint8_t *adv_data, uint8_t ble_evt_type);

esp_err_t esp_ble_config_ruuvi_data (esp_ble_ruuvi_vendor_t *vendor_config, esp_ble_ruuvi_t *ruuvi_adv_data);
