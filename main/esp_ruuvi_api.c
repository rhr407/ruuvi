#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "esp_log.h"

#include "esp_gap_ble_api.h"
#include "esp_ruuvi_api.h"


const uint8_t uuid_zeros[ESP_UUID_LEN_128] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

/* Constant part of ruuvi data */
esp_ble_ruuvi_head_t ruuvi_common_head = {
    .flags = {0x02, 0x01, 0x06},
    .length = 0x11,
    .type = 0xFF,
    .company_id = 0x0499,
    .beacon_type = 0x1234
};


/* Vendor part of ruuvi data*/
esp_ble_ruuvi_vendor_t vendor_config = {
    .proximity_uuid = ESP_UUID,
    .major = ENDIAN_CHANGE_U16(ESP_MAJOR), //Major=ESP_MAJOR
    .minor = ENDIAN_CHANGE_U16(ESP_MINOR), //Minor=ESP_MINOR
    .measured_power = 0xC5
};

bool esp_ble_is_ruuvi_packet (uint8_t *adv_data, uint8_t ble_event_type) {
    bool result = false;


    if ((adv_data != NULL) && (ble_event_type == 3)) {

        if (!memcmp(adv_data + 4, (uint8_t*) & (ruuvi_common_head) + 4, 3)) {
            result = true;
        }
    }

    return result;
}
esp_err_t esp_ble_config_ruuvi_data (esp_ble_ruuvi_vendor_t *vendor_config, esp_ble_ruuvi_t *ruuvi_adv_data) {
    if ((vendor_config == NULL) || (ruuvi_adv_data == NULL) || (!memcmp(vendor_config->proximity_uuid, uuid_zeros, sizeof(uuid_zeros)))) {
        return ESP_ERR_INVALID_ARG;
    }

    memcpy(&ruuvi_adv_data->ruuvi_head, &ruuvi_common_head, sizeof(esp_ble_ruuvi_head_t));
    memcpy(&ruuvi_adv_data->ruuvi_vendor, vendor_config, sizeof(esp_ble_ruuvi_vendor_t));

    return ESP_OK;
}


