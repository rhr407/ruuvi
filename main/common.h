#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "freertos/FreeRTOS.h"

#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"

#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_mqtt_client.h"
#include "aws_iot_shadow_interface.h"
#include "driver/gpio.h"

#include "cJSON.h"


#include "driver/spi_master.h"
#include "esp_spi_flash.h"
#include "mdns.h"
#include "lwip/api.h"
#include "lwip/err.h"
#include "lwip/netdb.h"


#include "wifi_manager.h"
#include "http_server.h"

#include <stdbool.h>
#include "esp_wifi_types.h"
#include "esp_log.h"

#include "json.h"
#include "http_server.h"
#include "wifi_manager.h"

#include "freertos/queue.h"

#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"

#include "driver/adc.h"
#include "esp_adc_cal.h"


#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"



#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"

#include "esp_event_loop.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_mqtt_client.h"
#include "aws_iot_shadow_interface.h"
#include "driver/gpio.h"

#include "cJSON.h"


#include "driver/spi_master.h"
#include "esp_spi_flash.h"
#include "mdns.h"
#include "lwip/api.h"
#include "lwip/netdb.h"


#include "wifi_manager.h"
#include "http_server.h"

#include "esp_wifi_types.h"

#include "json.h"

#include "freertos/queue.h"

#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"

#include "nvs.h"
#include "nvs_flash.h"

#include <time.h>
#include <sys/time.h>
#include "freertos/task.h"
#include "esp_attr.h"
#include "esp_sleep.h"

#include "lwip/err.h"
#include "apps/sntp/sntp.h"

#include <stdint.h>
#include <stdbool.h>


#include "esp_bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_main.h"
#include "esp_bt_defs.h"
#include "esp_ruuvi_api.h"

#include "esp_int_wdt.h"




extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");
extern const uint8_t server_cert_pem_end[] asm("_binary_ca_cert_pem_end");


extern EventGroupHandle_t wifi_manager_event_group;
extern const int WIFI_MANAGER_STA_DISCONNECT_BIT;
extern const int WIFI_MANAGER_WIFI_CONNECTED_BIT;

extern void tmp_task(void *pvParameters);

extern esp_err_t _http_event_handler(esp_http_client_event_t *evt);

extern void tmp_task_mine(void);

void blink_LED_task(void *pvParameter);


extern void ble_ruuvi_init(void);
extern void get_time_task(void *param);
extern void aws_iot_task(void *param);
